from math import sqrt, log

with open("data3.txt") as f:
	lines = f.read().splitlines()

data = [[float(d) for d in line.split()] for line in lines if line]

A_motSpeed = 0
A_motor = []
A_avgs = []
A_diff = []
RA_HIST_SECONDS = 25

B_motSpeed = 0
B_motor = []
B_arrs = []
B_arousal = 0.0
B_lastPressure = 0
B_peakStart = 0
B_decay = 0.995
B_limit = 200

C_motSpeed = 0
C_motor = []
C_arrs = []
C_arousal = 0.0
C_lastPressure = 0
C_peakStart = 0
C_decay = 0.9985
C_limit = 14#log(4000)#200**0.5

FREQUENCY = 10

raPressure = []


maxSpeed = 255
rampTimeS = 10

#millis	pressure	avgPressure	AtEdge?

xs = []

pressures = []

edges = []

for d in data:
	x = d[0]
	xs.append(x)

	pressure = d[1]
	pressures.append(pressure/10)

	edge = d[3]
	if edge:
		edges.append(x)

	raPressure = [pressure] + raPressure[:RA_HIST_SECONDS*FREQUENCY]

	limit = 200

	motIncrement = maxSpeed / (FREQUENCY * rampTimeS)

	# ============== ALGO: min peak from average (Rhobot) ===================
	avgPressure = sum(raPressure)/len(raPressure)
	A_avgs.append(avgPressure)
	A_diff.append(pressure-avgPressure)

	if pressure - avgPressure > limit:
		A_motSpeed = -0.5 * rampTimeS * FREQUENCY * motIncrement
	elif A_motSpeed < maxSpeed:
		A_motSpeed += motIncrement

	if A_motSpeed > 0:
		A_motor.append(A_motSpeed)
	else:
		A_motor.append(0)

	# ================ ALGO: sum of min peaks (Onwrikbaar) ======================
	#print(B_arousal, motSpeed, limit/10, pressure-peakStart)
	B_arrs.append(B_arousal)
	if pressure < B_lastPressure:
		if pressure > B_peakStart:
			if pressure - B_peakStart >= 5:#limit / 40:
				B_arousal += pressure - B_peakStart

		B_peakStart = pressure

	B_lastPressure = pressure

	if B_arousal > B_limit:
		B_motSpeed = -0.5 * rampTimeS * FREQUENCY * motIncrement
	elif B_motSpeed < maxSpeed:
		B_motSpeed += motIncrement

	B_arousal *= B_decay

	if B_motSpeed > 0:
		B_motor.append(B_motSpeed)
	else:
		B_motor.append(0)

	# =============== ALGO: sum of log of min peaks

	C_arrs.append(C_arousal)
	if pressure < C_lastPressure:
		if pressure > C_peakStart:
			if pressure - C_peakStart >= 5:#limit / 40:
				#https://en.wikipedia.org/wiki/Weber%E2%80%93Fechner_law
				#C_arousal += (pressure - C_peakStart)**0.5
				C_arousal += log(log(pressure - C_peakStart))
				#C_arousal += (pressure - C_peakStart)

		C_peakStart = pressure

	C_lastPressure = pressure

	if C_arousal > C_limit:
		C_motSpeed = -0.5 * rampTimeS * FREQUENCY * motIncrement
	elif C_motSpeed < maxSpeed:
		C_motSpeed += motIncrement

	C_arousal *= C_decay

	if C_motSpeed > 0:
		C_motor.append(C_motSpeed)
	else:
		C_motor.append(0)

import matplotlib.pyplot as plt

plt.plot(xs, pressures, label="pressure")

for edge in edges:
	plt.axvline(edge)

"""
plt.plot(xs, A_avgs, label="A_runningaverage")
#plt.plot(A_diff, label="A_diff")
plt.plot(xs, A_motor, label="A_motor")
plt.plot(xs, B_motor, label="B_motor")
plt.plot(xs, B_arrs, label="B_arousal")
plt.plot(xs, [B_limit for i in range(len(data))], label="B_limit")

"""
#plt.plot(xs, C_motor, label="C_motor")
plt.plot(xs, C_arrs, label="C_arousal")
#plt.plot(xs, [C_limit for i in range(len(data))], label="C_limit")
plt.legend()
plt.show()
